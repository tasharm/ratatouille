﻿using Ratatouille_DSL.Core.Output;
using Ratatouille_DSL.Core.Recipes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ratatouille_DSL.Compilers
{
    public interface IRecipeCompiler<T>
    {
        T GenerateOutput(IRecipeOutput r);
    }
}
