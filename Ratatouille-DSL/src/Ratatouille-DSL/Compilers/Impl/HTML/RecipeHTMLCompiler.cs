﻿using Ratatouille_DSL.Core.Ingredients;
using Ratatouille_DSL.Core.Measure;
using Ratatouille_DSL.Core.Output;
using Ratatouille_DSL.Core.Output.Impl;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Ratatouille_DSL.Compilers.HTML.Impl
{
    public class RecipeHTMLCompiler : IRecipeCompiler<string>
    {
        private string template = @"<head><body><h1>{0}</h1>{1}{2}{3}{4}<body></head>";

        public string GenerateOutput(IRecipeOutput r)
        {
            var Name = r.Name;
            var ingredients = TemplatizeIngredients(r.Ingredients);
            var preRequisites = TemplatizePreReqs(r.DependsOn);
            var steps = TemplatizeSteps(r.Steps);
            var yieldInformation = TemplatizeYield(r.Yields);

            var html = string.Format(template, Name, ingredients, preRequisites, steps, yieldInformation);

            var path = Path.Combine(Directory.GetCurrentDirectory(), $"{Name}.html");

            using (StreamWriter sw = File.CreateText(path))
            {
                sw.Write(html);
            }

            return path;
        }

        private object TemplatizePreReqs(IList<IRecipeOutput> dependsOn)
        {
            if (dependsOn.Count == 0) { return string.Empty; }
            string stepsTemplate = @"<h2>Pre-requisites</h2><ul>{0}</ul>";
            var stringBuilder = new StringBuilder();
            foreach (var dependency in dependsOn)
            {
                stringBuilder.Append(TemplatizeDependency(dependency));
            }
            return string.Format(stepsTemplate, stringBuilder.ToString());
        }

        private string TemplatizeDependency(IRecipeOutput dependency)
        {
            string template = @"<h2>{0}</h2><ul>{1}</ul>";
            var stringBuilder = new StringBuilder();
            foreach (var step in dependency.Steps)
            {
                stringBuilder.Append(TemplatizeStep(step));
            }

            return string.Format(template, dependency.Name, stringBuilder.ToString());
        }

        private string TemplatizeYield(Measurement yields)
        {
            string template = @"<h3>Yields</h3>{0}";
            return string.Format(template, yields.GetDisplayString());
        }

        internal string TemplatizeSteps(IList<IRecipeOutputStep> steps)
        {
            string stepsTemplate = @"<h2>Steps</h2><ul>{0}</ul>";
            var stringBuilder = new StringBuilder();
            foreach (var step in steps)
            {
                stringBuilder.Append(TemplatizeStep(step));
            }
            return string.Format(stepsTemplate, stringBuilder.ToString());
        }

        internal string TemplatizeIngredients(IList<IIngredient> ingredients)
        {
            string ingredientsTemplate = @"<h2>Ingredients</h2><ul>{0}</ul>";
            string ingredientListItemTemplate = @"<li>{0}</li>";
            var stringBuilder = new StringBuilder();
            foreach (var ingredient in ingredients) 
            {
                stringBuilder.Append(
                    string.Format(
                        ingredientListItemTemplate, 
                        $"{ingredient.Name} - {ingredient.Measure.GetDisplayString()}."));
            }
            return string.Format(ingredientsTemplate, stringBuilder.ToString());
        }

        internal string TemplatizeStep(IRecipeOutputStep recipeOutputStep)
        {
            string template = @"<li>{0}</li>";
            switch (recipeOutputStep.StepType)
            {
                case StepType.Add:
                    var step = recipeOutputStep as AddStep;
                    return string.Format(template, $"Add {step.Ingredient.Measure.GetDisplayString()} of {step.Ingredient.Name}.");
                case StepType.Process:
                    var procesStep = recipeOutputStep as ProcessStep;
                    return string.Format(template, $"{procesStep.Name} for {procesStep.Until.GetDisplayTime()}.");
                case StepType.StartWith:
                    var startWithStep = recipeOutputStep as StartWithStep;
                    return string.Format(template, $"Take a {startWithStep.Vessel.DisplayName}.");
                default:
                    throw new NotImplementedException();
            }
        }
    }
}
