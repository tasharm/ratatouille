﻿using Ratatouille_DSL.Core.Output.Impl;
using Ratatouille_DSL.Core.Timers;
using Ratatouille_DSL.Core.Vessels;

namespace Ratatouille_DSL.Core.Recipes.Impl
{
    public class GrindingRecipe : Recipe<GrindingRecipe>
    {
        public GrindingRecipe(IVessel vessel)
            : base(RecipeType.Grinding, vessel)
        {
        }

        public GrindingRecipe GrindFor(Timer timer)
        {
            this.Steps.Add(new GrindingStep(timer));
            return this;
        }
    }
}
