﻿using Ratatouille_DSL.Core.Ingredients;
using Ratatouille_DSL.Core.Ingredients.Impl;
using Ratatouille_DSL.Core.Measure;
using Ratatouille_DSL.Core.Output;
using Ratatouille_DSL.Core.Output.Impl;
using Ratatouille_DSL.Core.Vessels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleToAttribute("Ratatouille-DSL.Tests")]
namespace Ratatouille_DSL.Core.Recipes.Impl
{
    public class Recipe<T>
        where T : Recipe<T>
    {
        internal IVessel Vessel { get; private set; }

        internal IList<IIngredient> Ingredients { get; set; } = new List<IIngredient>();

        internal IList<IRecipeOutput> DependsOn { get; set; } = new List<IRecipeOutput>();

        internal IList<IRecipeOutputStep> Steps { get; private set; } = new List<IRecipeOutputStep>();

        internal Measurement MinimumYield { get; private set; }

        public RecipeType Type { get; set; }

        internal Recipe() { }

        internal Recipe(RecipeType type, IVessel vessel)
        {
            Type = type;
            Vessel = vessel;
            Steps.Add(new StartWithStep(vessel));
        }

        public virtual T Add(IIngredient ingredient)
        {
            AddInternal(ingredient);

            this.Steps.Add(new AddStep(ingredient));
            return (T)this;
        }

        private void AddInternal(IIngredient ingredient)
        {
            var existing = Ingredients.Where(i => string.Equals(i.Name, ingredient.Name)).FirstOrDefault();
            if (existing != default)
            {
                existing.Measure.Amount += ingredient.Measure.Amount;
            }
            else
            {
                Ingredients.Add(ingredient);
            }
        }

        public virtual T Add(Tuple<IRecipeOutput, Measurement> savedRecipe)
        {
            var recipe = savedRecipe.Item1;
            var measure = savedRecipe.Item2;
            MergeIngredients(recipe);
            DependsOn.Add(recipe);
            Steps.Add(new AddStep(new Ingredient(recipe.Name, measure)));
            return (T)this;
        }

        public IRecipeOutput Finish(string name, Measurement yield)
        {
            
            return new RecipeOutput()
            {
                Name = name,
                Yields = yield,
                DependsOn = DependsOn,
                Ingredients = Ingredients,
                Vessel = Vessel,
                Steps = Steps
            };
        }

        internal void MergeIngredients(IRecipeOutput recipe)
        {
            if (recipe is null)
            {
                throw new ArgumentNullException(nameof(recipe));
            }

            foreach (var ingredient in recipe.Ingredients)
            {
                AddInternal(ingredient);
            }
        }

        public virtual T StartWith(IVessel vessel)
        {
            if (this.Vessel != null)
            {
                throw new ArgumentException($"cannot updated vessel once chosen.");
            }

            if (vessel.UsedFor != Type)
            {
                throw new ArgumentException($"Vessel {vessel.DisplayName} cannot be used for recipe of type {Type}.");
            }

            Vessel = vessel;
            return (T)this;
        }
    }
}
