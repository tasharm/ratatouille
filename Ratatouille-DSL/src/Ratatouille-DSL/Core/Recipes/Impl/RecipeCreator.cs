﻿using Ratatouille_DSL.Core.Vessels.Impl;
using System;

namespace Ratatouille_DSL.Core.Recipes.Impl
{
    public class RecipeCreator
    {
        public static HeatingRecipe HeatingRecipe(VesselType vesselType)
        {
            var vessel = VesselFactory.GetVessel(vesselType);
            if (vessel.UsedFor != RecipeType.Heating)
            {
                throw new ArgumentException($"Vessel type {vesselType} cannot be used for Heating recipes.");
            }

            return new HeatingRecipe(vessel);
        }

        public static GrindingRecipe GrindingRecipe(VesselType vesselType)
        {
            var vessel = VesselFactory.GetVessel(vesselType);
            if (vessel.UsedFor != RecipeType.Grinding)
            {
                throw new ArgumentException($"Vessel type {vesselType} cannot be used for Grinding recipes.");
            }

            return new GrindingRecipe(vessel);
        }
    }
}
