﻿namespace Ratatouille_DSL.Core.Recipes.Impl
{
    public enum RecipeType
    {
        Baking,
        Heating,
        Grinding,
    }
}