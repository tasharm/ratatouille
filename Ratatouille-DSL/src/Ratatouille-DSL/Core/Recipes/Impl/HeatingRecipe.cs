﻿using Ratatouille_DSL.Core.Ingredients;
using Ratatouille_DSL.Core.Output.Impl;
using Ratatouille_DSL.Core.Timers;
using Ratatouille_DSL.Core.Vessels;

namespace Ratatouille_DSL.Core.Recipes.Impl
{
    public class HeatingRecipe : Recipe<HeatingRecipe>
    {
        public HeatingRecipe(IVessel vessel)
            : base(RecipeType.Heating, vessel)
        {
        }

        internal HeatingRecipe()
            : base()
        { }

        public HeatingRecipe HeatFor(Timer timer)
        {
            this.Steps.Add(new HeatingStep(timer));
            return this;
        }
    }
}
