﻿

namespace Ratatouille_DSL.Core.Output
{
    public interface IRecipeOutputStep
    {
        public StepType StepType { get; set; }
    }
}
