﻿using Ratatouille_DSL.Core.Vessels;

namespace Ratatouille_DSL.Core.Output.Impl
{
    public class StartWithStep : IRecipeOutputStep
    {
        public StartWithStep(IVessel vessel)
        {
            Vessel = vessel;
        }

        public StepType StepType { get; set; } = StepType.StartWith;

        public IVessel Vessel { get; set; }
    }
}
