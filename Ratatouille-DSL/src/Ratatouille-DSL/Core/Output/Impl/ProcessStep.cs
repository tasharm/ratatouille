﻿using Ratatouille_DSL.Core.Timers;

namespace Ratatouille_DSL.Core.Output.Impl
{
    public abstract class ProcessStep : IRecipeOutputStep
    {
        public ProcessStep(string name, Timer until)
        {
            Name = name;
            Until = until;
        }
        public string Name { get; set; }

        public Timer Until { get; set; }

        public StepType StepType { get; set; } = StepType.Process;

        //TODO : Support condition based process step.
    }
}
