﻿using Ratatouille_DSL.Core.Ingredients;
using Ratatouille_DSL.Core.Ingredients.Impl;
using Ratatouille_DSL.Core.Measure;
using Ratatouille_DSL.Core.Vessels;
using System.Collections.Generic;

namespace Ratatouille_DSL.Core.Output.Impl
{
    public class RecipeOutput : IRecipeOutput
    {
        public IList<IIngredient> Ingredients { get; set; }
        public string Name { get; set; }
        public IList<IRecipeOutputStep> Steps { get; set; }
        public Measurement Yields { get; set; }
        public IVessel Vessel { get; set; }
        public IList<IRecipeOutput> DependsOn { get; set; } = new List<IRecipeOutput>();

        public RecipeOutput() { }

        public RecipeOutput(IRecipeOutput existingRecipe, int units = 1)
        {
            Ingredients = Multiples(existingRecipe.Ingredients, units);
            Name = existingRecipe.Name;
            Steps = existingRecipe.Steps;

            // TODO : multiply yields as well ?
            Yields = existingRecipe.Yields;

            // TODO : Add validation if multiples can be accomodated in the default vessel used.
            Vessel = existingRecipe.Vessel;
        }

        internal IList<IIngredient> Multiples(IList<IIngredient> ingredients, int num)
        {
            var ingredientsList = new List<IIngredient>(ingredients.Count);

            foreach (var ingredient in ingredients)
            {
                ingredientsList.Add(
                    new Ingredient(
                        ingredient.Name,
                        new Measurement(ingredient.Measure.Amount * num, ingredient.Measure.AmountInUnits * num, ingredient.Measure.Unit)));
            }
            return ingredientsList;
        }
    }
}
