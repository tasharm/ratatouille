﻿using Ratatouille_DSL.Core.Timers;

namespace Ratatouille_DSL.Core.Output.Impl
{
    public class GrindingStep : ProcessStep
    {
        public GrindingStep(Timer timer)
            : base("Grind", timer)
        {
        }
    }
}
