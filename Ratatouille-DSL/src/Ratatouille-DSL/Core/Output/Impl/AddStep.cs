﻿using Ratatouille_DSL.Core.Ingredients;

namespace Ratatouille_DSL.Core.Output.Impl
{
    public class AddStep : IRecipeOutputStep
    {
        public AddStep(IIngredient ingredient)
        {
            Ingredient = ingredient;
        }

        public IIngredient Ingredient { get; set; }

        public StepType StepType { get; set; } = StepType.Add;
    }
}