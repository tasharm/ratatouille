﻿using Ratatouille_DSL.Core.Timers;

namespace Ratatouille_DSL.Core.Output.Impl
{
    public class HeatingStep : ProcessStep
    {
        public HeatingStep(Timer timer)
            : base("Heat", timer)
        { }
    }
}
