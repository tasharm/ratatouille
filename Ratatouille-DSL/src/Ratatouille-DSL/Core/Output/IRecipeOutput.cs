﻿using Ratatouille_DSL.Core.Ingredients;
using Ratatouille_DSL.Core.Measure;
using Ratatouille_DSL.Core.Vessels;
using System.Collections.Generic;

namespace Ratatouille_DSL.Core.Output
{
    public interface IRecipeOutput
    {
        IList<IIngredient> Ingredients { get; set; }

        string Name { get; set; }

        IList<IRecipeOutput> DependsOn { get; set; }

        IList<IRecipeOutputStep> Steps { get; set; }

        Measurement Yields { get; set; }

        IVessel Vessel { get; set; }
    }
}
