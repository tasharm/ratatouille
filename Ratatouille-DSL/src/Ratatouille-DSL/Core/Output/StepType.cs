﻿namespace Ratatouille_DSL.Core.Output
{
    public enum StepType
    {
        Add,
        Process,
        StartWith
    }
}