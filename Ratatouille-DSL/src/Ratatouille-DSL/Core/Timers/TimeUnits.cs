﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ratatouille_DSL.Core.Timers
{
    public enum TimeUnits
    {
        Seconds,
        Minutes
    }
}
