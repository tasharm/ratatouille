﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ratatouille_DSL.Core.Timers
{
    public class Timer
    {
        private TimeSpan timeSpan;

        public string GetDisplayTime()
        {
            switch (Unit)
            {
                case TimeUnits.Seconds:
                    return $"{this.timeSpan.TotalSeconds} seconds";
                case TimeUnits.Minutes:
                    return $"{this.timeSpan.Minutes} minutes";
                default:
                    throw new NotImplementedException();
            }
        }

        private TimeUnits Unit;

        public Timer(TimeSpan timespan, TimeUnits unit)
        {
            this.timeSpan = timespan;
            this.Unit = unit;
        }
    }
}
