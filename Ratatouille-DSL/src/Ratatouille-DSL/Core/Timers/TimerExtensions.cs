﻿using System;

namespace Ratatouille_DSL.Core.Timers
{
    public static class TimerExtensions
    {
        public static Timer mins(this int num)
        {
            return new Timer(TimeSpan.FromMinutes(num), TimeUnits.Minutes);
        }

        public static Timer seconds(this int num)
        {
            return new Timer(TimeSpan.FromSeconds(num), TimeUnits.Seconds);
        }
    }
}