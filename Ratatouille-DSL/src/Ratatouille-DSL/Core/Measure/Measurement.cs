﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ratatouille_DSL.Core.Measure
{
    public class Measurement
    {
        /// <summary>
        /// Represents the number of teaspoons.
        /// </summary>
        public double Amount { get; set; }

        public double AmountInUnits { get; set; }

        public Measurement(double amountInTsp, double amountInUnits, string unit)
        {
            Amount = amountInTsp;
            AmountInUnits = amountInUnits;
            Unit = unit;
        }

        public string Unit { get; set; }

        public string GetDisplayString()
        {
            return $"{AmountInUnits} {Unit}";
        }
    }
}
