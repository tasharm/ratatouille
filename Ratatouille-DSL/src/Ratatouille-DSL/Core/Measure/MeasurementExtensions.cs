﻿using Ratatouille_DSL.Core.Ingredients;
using Ratatouille_DSL.Core.Ingredients.Impl;
using Ratatouille_DSL.Core.Output;
using Ratatouille_DSL.Core.Output.Impl;
using System;

namespace Ratatouille_DSL.Core.Measure
{
    public static class MeasurementExtensions
    {
        public static Measurement cups(this double quantity)
        {
            return new Measurement(quantity * 48, quantity, "cups");
        }

        public static Measurement tbsp(this double quantity)
        {
            return new Measurement(quantity * 3, quantity, "tbsp");
        }

        public static Measurement tsp(this double quantity)
        {
            return new Measurement(quantity, quantity, "tsp");
        }

        public static Measurement cups(this int quantity)
        {
            return new Measurement(quantity * 48, quantity, "cups");
        }

        public static Measurement tbsp(this int quantity)
        {
            return new Measurement(quantity * 3, quantity, "tbsp");
        }

        public static Measurement tsp(this int quantity)
        {
            return new Measurement(quantity, quantity, "tsp");
        }

        public static IIngredient of(this Measurement measure, string ingredientName)
        {
            return new Ingredient(ingredientName, measure);
        }

        public static Tuple<IRecipeOutput, Measurement> of(this Measurement measure, IRecipeOutput recipeToUse)
        {
            if (recipeToUse.Yields == null)
            {
                throw new ArgumentException($"Cannot use ${recipeToUse.Name} as it does not have a minimum yield configured. Call finish method on the input recipe and try again.");
            }

            int units = (int)Math.Ceiling(measure.Amount / recipeToUse.Yields.Amount);
            return Tuple.Create(new RecipeOutput(recipeToUse, units) as IRecipeOutput, measure);
        }
    }
}
