﻿using Ratatouille_DSL.Core.Measure;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;

namespace Ratatouille_DSL.Core.Ingredients.Impl
{
    public static class CommonIngredients
    {
        public static IIngredient Water(this Measurement measure)
        {
            return new Ingredient("water", measure);
        }

        public static IIngredient Sugar(this Measurement measure)
        {
            return new Ingredient("sugar", measure);
        }

        public static IIngredient Milk(this Measurement measure)
        {
            return new Ingredient("milk", measure);
        }
    }
}
