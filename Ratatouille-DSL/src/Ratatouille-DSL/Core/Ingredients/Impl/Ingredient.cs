﻿using Ratatouille_DSL.Core.Measure;

namespace Ratatouille_DSL.Core.Ingredients.Impl
{
    public class Ingredient : IIngredient
    {
        public string Name { get ; set ; }
        public Measurement Measure { get; set; }

        public Ingredient(string name, Measurement measure)
        {
            this.Name = name;
            this.Measure = measure;
        }
    }
}
