﻿using Ratatouille_DSL.Core.Measure;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ratatouille_DSL.Core.Ingredients
{
    public interface IIngredient
    {
        string Name { get; set; }

        Measurement Measure { get; set; }
    }
}
