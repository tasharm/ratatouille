﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ratatouille_DSL.Core.Vessels.Impl
{
    public class VesselFactory
    {
        public static IVessel GetVessel(VesselType type)
        {
            switch (type)
            {
                case VesselType.SaucePan:
                    return new SaucePan();
                case VesselType.Grinder:
                    return new Grinder();
                default:
                    throw new NotImplementedException();
            }
        }
    }
}
