﻿using Ratatouille_DSL.Core.Measure;
using Ratatouille_DSL.Core.Recipes.Impl;

namespace Ratatouille_DSL.Core.Vessels.Impl
{
    public class SaucePan : IVessel
    {
        public RecipeType UsedFor { get; set; } = RecipeType.Heating;
        public Measurement Capacity { get; set; } = 5.cups();
        public string DisplayName { get; set; } = "Sauce Pan";
    }
}
