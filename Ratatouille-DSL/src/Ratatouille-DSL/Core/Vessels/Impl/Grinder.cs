﻿using Ratatouille_DSL.Core.Measure;
using Ratatouille_DSL.Core.Recipes.Impl;

namespace Ratatouille_DSL.Core.Vessels.Impl
{
    public class Grinder : IVessel
    {
        public RecipeType UsedFor { get; set; } = RecipeType.Grinding;
        public Measurement Capacity { get; set; } = 5.cups();
        public string DisplayName { get; set; } = "Grinder";
    }
}
