﻿using Ratatouille_DSL.Core.Measure;
using Ratatouille_DSL.Core.Recipes.Impl;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ratatouille_DSL.Core.Vessels
{
    public interface IVessel
    {
        RecipeType UsedFor { get; set; }

        Measurement Capacity { get; set; }

        string DisplayName { get; set; }
    }
}
