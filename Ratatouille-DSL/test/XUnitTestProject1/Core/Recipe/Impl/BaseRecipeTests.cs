﻿
using FluentAssertions;
using Ratatouille_DSL.Compilers.HTML.Impl;
using Ratatouille_DSL.Core.Ingredients;
using Ratatouille_DSL.Core.Ingredients.Impl;
using Ratatouille_DSL.Core.Measure;
using Ratatouille_DSL.Core.Output;
using Ratatouille_DSL.Core.Output.Impl;
using Ratatouille_DSL.Core.Recipes.Impl;
using Ratatouille_DSL.Core.Timers;
using Ratatouille_DSL.Core.Vessels.Impl;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using Xunit;

namespace Ratatouille_DSL.tests.Core.Recipes
{
    public class RecipeTests
    {

        [Fact]
        public void Add_AddsNewIngredientToIngredientsList()
        {
            IList<IIngredient> expectedIngredients = new List<IIngredient>()
            {
                new Ingredient("foo", new Measurement(5.0, 5.0, "tsp"))
            };

            var recipe = RecipeCreator.HeatingRecipe(VesselType.SaucePan)
                .Add(5.tsp().of("foo"));

            recipe.Ingredients.Should().Equals(expectedIngredients);
        }

        [Fact]
        public void Add_Yields_ShouldSetMinYield()
        {
            Measurement expectedMinServings = new Measurement(10, 10, "tsp");

            var recipe = RecipeCreator.HeatingRecipe(VesselType.SaucePan)
                .Add(5.tsp().of("grated ginger"))
                .Add(5.tsp().of("grated garlic"))
                .HeatFor(5.mins())
                .Finish("abc", 1.tsp());

            recipe.Yields.Should().Equals(expectedMinServings);
        }

        [Fact]
        public void Add_ExistingRecipe_ShouldMergeIngredients()
        {
            var existingRecipe = RecipeCreator.HeatingRecipe(VesselType.SaucePan)
                .Add(5.tsp().of("ing1"))
                .Add(6.tsp().of("ing2"))
                .Finish("foo", 11.tsp());

            var r = RecipeCreator.HeatingRecipe(VesselType.SaucePan)
                .Add(5.tsp().of("ing1"))
                .Add(2.tsp().of(existingRecipe));

            r.Ingredients.Count().Should().Be(2);
            r.Ingredients.First().Name.Should().Be("ing1");
            r.Ingredients.First().Measure.Amount.Should().Be(10);

            r.Ingredients[1].Name.Should().Be("ing2");
            r.Ingredients[1].Measure.Amount.Should().Be(6);
        }

        [Fact]
        public void Add_ExistingRecipe_MultipleUnits_MergesIngredients()
        {
            // start with the recipe which produces minimum of 11 tsps.
            var existingRecipe = RecipeCreator.HeatingRecipe(VesselType.SaucePan)
                .Add(5.tsp().of("ing1"))
                .Add(6.tsp().of("ing2"))
                .Finish("foo", 11.tsp());

            // taking more than the minimum yield.
            var r = RecipeCreator.HeatingRecipe(VesselType.SaucePan)
                .Add(5.tsp().of("ing1"))
                .Add(15.tsp().of(existingRecipe));  // using more than the minimum yield.

            r.Ingredients.Count().Should().Be(2);
            r.Ingredients.First().Name.Should().Be("ing1");
            r.Ingredients.First().Measure.Amount.Should().Be(15);

            r.Ingredients[1].Name.Should().Be("ing2");
            r.Ingredients[1].Measure.Amount.Should().Be(12);
        }
    }
}