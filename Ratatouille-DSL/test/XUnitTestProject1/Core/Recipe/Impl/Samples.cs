﻿using FluentAssertions;
using Ratatouille_DSL.Compilers.HTML.Impl;
using Ratatouille_DSL.Core.Ingredients.Impl;
using Ratatouille_DSL.Core.Measure;
using Ratatouille_DSL.Core.Recipes.Impl;
using Ratatouille_DSL.Core.Timers;
using Ratatouille_DSL.Core.Vessels.Impl;
using System;
using System.Diagnostics;
using System.Linq;
using Xunit;

namespace Ratatouille_DSL.Tests.Core.Recipe.Impl
{
    public class Samples
    {
        const string edgeLocation = @"C:\Program Files (x86)\Microsoft\Edge\Application\msedge.exe";

        [Fact]
        public void SimpleRecipe_Sucess()
        {
            var simpleRecipe = RecipeCreator.HeatingRecipe(VesselType.SaucePan)
                .Add(2.cups().Water())
                .HeatFor(5.mins())
                .Finish("Warm_Water", 2.cups());

            // A simple client which parses the IR representation to generate HTML instructions to cook the recipe.
            var output = new RecipeHTMLCompiler();
            Process.Start(edgeLocation, @output.GenerateOutput(simpleRecipe));
        }

        [Fact]
        public void RecipeReuse_Success()
        {
            // pre-requisite for the tea recipe.
            var teaMasala = RecipeCreator.GrindingRecipe(VesselType.Grinder)
                .Add(1.tbsp().of("ginger powder"))
                .Add(1.tsp().of("cinnamon"))
                .Add(0.5.cups().of("cloves"))
                .GrindFor(2.mins())
                .Finish("Tea_masala", 0.5.cups());

            // tea recipe
            var masalaChai = RecipeCreator.HeatingRecipe(VesselType.SaucePan)
                .Add(1.cups().Water())
                .Add(2.tbsp().of("tea"))
                .Add(2.tbsp().of(teaMasala))
                .HeatFor(5.mins())
                .Add(0.5.cups().Milk())
                .HeatFor(5.mins())
                .Finish("Masala_chai", 2.cups());

            // A simple client which parses the IR representation to generate HTML instructions to cook the recipe.
            var output = new RecipeHTMLCompiler();
            Process.Start(edgeLocation, output.GenerateOutput(masalaChai));
        }

        [Fact]
        public void RecipeReuseMultiples_Success()
        {
            // pre-requisite for the tea recipe.
            var teaMasala = RecipeCreator.GrindingRecipe(VesselType.Grinder)
                .Add(1.tbsp().of("ginger powder"))
                .Add(1.tsp().of("cinnamon"))
                .Add(0.5.cups().of("cloves"))
                .GrindFor(2.mins())
                .Finish("Tea_masala", 0.5.cups());

            // tea recipe
            var masalaChai = RecipeCreator.HeatingRecipe(VesselType.SaucePan)
                .Add(1.cups().Water())
                .Add(2.tbsp().of("tea"))
                .Add(2.cups().of(teaMasala))    // using 4 times the quantity produced in the recipe
                .HeatFor(5.mins())
                .Add(0.5.cups().Milk())
                .HeatFor(5.mins())
                .Finish("Masala_chai", 2.cups());

            masalaChai.Ingredients.Where(i => i.Name == "cinnamon").First().Measure.Should().BeEquivalentTo(4.tsp());

            // A simple client which parses the IR representation to generate HTML instructions to cook the recipe.
            var output = new RecipeHTMLCompiler();
            Process.Start(edgeLocation, output.GenerateOutput(masalaChai));
        }

        [Fact]
        public void InvalidVesselType_Throws()
        {
            Assert.Throws<ArgumentException>(() => RecipeCreator.HeatingRecipe(VesselType.Grinder));
        }
    }
}
