﻿using FluentAssertions;
using Ratatouille_DSL.Core.Measure;
using System.Collections.Generic;
using Xunit;

namespace Ratatouille_DSL.Tests.Core.Recipe.Impl
{
    public class MeasurementExtensionsTests
    {
        public static IEnumerable<object[]> ExtensionsTestData()
        {
            yield return new object[] { 1.5.tsp(), new Measurement(1.5, 1.5, "tsp") };
            yield return new object[] { 5.tsp(), new Measurement(5, 5, "tsp") };

            yield return new object[] { 5.5.tbsp(), new Measurement(16.5, 5.5, "tbsp") };
            yield return new object[] { 5.tbsp(), new Measurement(15, 5, "tbsp") };

            yield return new object[] { 2.5.cups(), new Measurement(120, 2.5, "cups") };
            yield return new object[] { 2.cups(), new Measurement(96, 2, "cups") };

        }

        [Theory]
        [MemberData(nameof(ExtensionsTestData))]
        public void DoubleExtensions_ValidateMeasurementCreated(Measurement input, Measurement expected)
        {
            expected.Should().BeEquivalentTo(input);
        }
    }
}
