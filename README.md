# Ratatouille
A DSL to author food recipes.

Initial proposal document : https://docs.google.com/document/d/17mHeqY12-tDxjFrLhXbamE9fm9eXptDtJS9u2u3IdjU

Design document : https://docs.google.com/document/d/1qLAiOCoxOGWgx21tGhuhOsw-asBNYgqpa4dSgCQSM-Y

Final Report : https://docs.google.com/document/d/1e3s5vYvAR9xOhkM8VOFDKKKWFFx71fyhjcdbV8Eh6Mo

Poster presentation : https://docs.google.com/presentation/d/1nilFwDuQrjYSN7XzyzpRAR9u0peue-Cwy4KCHgOkqYY

Demo video link : https://photos.google.com/share/AF1QipNxYZosIussrd1ZWZmF7drzo3IDKZ8IjVHStzgjWHNC51_qOdcecCzVZHMsJnXjcg?key=NzNURnItWVJvNV8xT0RSUkN5YU1ENnJWWXVGSS1n

Samples : https://bitbucket.org/tasharm/ratatouille/src/master/Ratatouille-DSL/test/XUnitTestProject1/Core/Recipe/Impl/Samples.cs
